package cl.duoc.pichangaspiscolas;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cl.duoc.pichangaspiscolas.Clases.Usuario;

public class LoginActivity extends AppCompatActivity {

    private List<Usuario> usuarios = new ArrayList<>();
    private EditText etUsuario, etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUsuarios();
        initView();
    }

    private void initView(){
        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = etUsuario.getText().toString();
                String password = etPassword.getText().toString();
                if(usuario.isEmpty() || password.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Ingrese los datos", Toast.LENGTH_SHORT).show();
                }else{
                    if(login(usuario, password)){
                        Toast.makeText(getApplicationContext(), "Bienvenido " + usuario, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra("usuario", usuario);
                        startActivity(i);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private boolean login(String usuario, String password) {
        for (Usuario u: usuarios) {
            if (u.getUsuario().equals(usuario) && u.getPassword().equals(password)){
                return true;
            }
        }
        return false;

        /*for (Usuario u: usuarios){
            if(u.getUsuario().length()>1 && u.getUsuario().length()>10)&& u.getPassword().equals(password){
                return true;
            }
        }*/
    }

    private void initUsuarios(){
        usuarios.add(new Usuario("bade", "1234"));
        usuarios.add(new Usuario("campos", "1234"));
        usuarios.add(new Usuario("carrasco", "1234"));
        usuarios.add(new Usuario("monje", "1234"));
        usuarios.add(new Usuario("farias", "1234"));
    }
}
