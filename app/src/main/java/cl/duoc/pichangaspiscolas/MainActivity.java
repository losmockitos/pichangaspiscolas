package cl.duoc.pichangaspiscolas;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.List;

import cl.duoc.pichangaspiscolas.Adapters.PartidosAdapter;
import cl.duoc.pichangaspiscolas.Clases.Partido;
import cl.duoc.pichangaspiscolas.Clases.Usuario;

public class MainActivity extends AppCompatActivity {

    private Button btnCrearPartido;
    private RecyclerView listView;
    private PartidosAdapter partidosAdapter;
    private List<Partido> partidoList = new ArrayList<>();
    private String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        partidosAdapter.notifyDataSetChanged();
    }

    private void initView()
    {
        Intent intent = getIntent();
        usuario = intent.getStringExtra("usuario");
        btnCrearPartido = (Button) findViewById(R.id.button);
        btnCrearPartido.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FichaPartidoActivity.class);
                startActivity(i);
            }


        });
        listView = (RecyclerView) findViewById(R.id.listView);

        partidosAdapter = new PartidosAdapter(BaseDeDatos.getPartidos(), usuario);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(layoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(partidosAdapter);

        partidosAdapter.notifyDataSetChanged();
        loadTestData();
    }

    private void loadTestData() {
        List<Usuario> usuarioList = new ArrayList<>();
        usuarioList.add(new Usuario("bade", "1234"));
        usuarioList.add(new Usuario("campos", "1234"));
        usuarioList.add(new Usuario("carrasco", "1234"));
        usuarioList.add(new Usuario("monje", "1234"));
        usuarioList.add(new Usuario("farias", "1234"));
        usuarioList.add(new Usuario("u1", "1234"));
        usuarioList.add(new Usuario("u2", "1234"));
        usuarioList.add(new Usuario("u3", "1234"));
        usuarioList.add(new Usuario("u4", "1234"));
        usuarioList.add(new Usuario("u5", "1234"));
        usuarioList.add(new Usuario("u6", "1234"));
        usuarioList.add(new Usuario("u7", "1234"));
        usuarioList.add(new Usuario("u8", "1234"));

        partidoList.add(new Partido("Rancho Memo", "20/12/2017", "17:00"));
        Partido partido2 = new Partido("Estadio Municipal", "14/12/2017", "12:00");
        partido2.setUsuarioList(usuarioList);
        partidoList.add(partido2);
        partidoList.add(new Partido("Cancha Plaza Nueva", "29/11/2017", "20:00"));
        partidosAdapter.notifyDataSetChanged();
    }
}

