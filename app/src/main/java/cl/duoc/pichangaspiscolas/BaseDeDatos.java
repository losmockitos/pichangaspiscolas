package cl.duoc.pichangaspiscolas;

import java.util.ArrayList;

import cl.duoc.pichangaspiscolas.Clases.Partido;

/**
 * Created by user on 11-11-17.
 */

public class BaseDeDatos {

    private static ArrayList<Partido> misDatos = new ArrayList<>();

    public static ArrayList<Partido> getPartidos(){
        return misDatos;
    }

    public static boolean isAgregarPartido(Partido nuevoPartido) {
        if (buscarPartido(nuevoPartido.getNombrePartido()) == null) {
            misDatos.add(nuevoPartido);
            return true;
        } else {
            return false;
        }
    }

    public static Partido buscarPartido(String nombre) {
        Partido retorno = null;
        for (Partido aux : misDatos) {
            if (aux.getNombrePartido().equals(nombre)) {
                retorno = aux;
                break;
            }
        }
        return retorno;
    }

    public static boolean eliminarPartido(String nombre){
        for (int x = 0; x < misDatos.size(); x++) {
            if (misDatos.get(x).getNombrePartido().equals(nombre)) {
                misDatos.remove(x);
                return true;
            }
        }
        return false;
    }
}
