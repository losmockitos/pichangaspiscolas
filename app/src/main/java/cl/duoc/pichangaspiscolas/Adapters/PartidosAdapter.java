package cl.duoc.pichangaspiscolas.Adapters;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cl.duoc.pichangaspiscolas.Clases.Partido;
import cl.duoc.pichangaspiscolas.Clases.Usuario;
import cl.duoc.pichangaspiscolas.FichaPartidoActivity;
import cl.duoc.pichangaspiscolas.MainActivity;
import cl.duoc.pichangaspiscolas.R;

/**
 * Created by Pablo on 15-11-2017.
 */

public class PartidosAdapter extends RecyclerView.Adapter<PartidosAdapter.MyViewHolder>{
    private List<Partido> partidoList;
    private String usuario;

    public PartidosAdapter(List<Partido> partidoList, String usuario){
        this.partidoList = partidoList;
        this.usuario = usuario;
    }


        public class MyViewHolder extends RecyclerView.ViewHolder{
            public TextView nombre, fecha, hora;
            public ImageButton btnAceptar, btnRechazar;
            public Button btnUsuarios;

            public MyViewHolder(View v){
                super(v);
                nombre = (TextView) v.findViewById(R.id.tvNombre);
                fecha = (TextView) v.findViewById(R.id.tvFecha);
                hora = (TextView) v.findViewById(R.id.tvHora);

                btnAceptar = (ImageButton) v.findViewById(R.id.btnAceptar);
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Partido partido = partidoList.get(getAdapterPosition());
                        List<Usuario> usuarioList = partido.getUsuarioList();
                        boolean exist = false;
                        if(usuarioList.size() > 0){
                            for (Usuario u: usuarioList) {
                                String uNombre = u.getUsuario();
                                if (uNombre.equals(usuario)){
                                    exist = true;
                                }
                            }
                        }
                        if (!exist){
                            boolean added = partido.agregarUsuario(new Usuario(usuario));
                            if (added){
                                Toast.makeText(view.getContext(), "Aceptado :)", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(view.getContext(), "Sin Cupos :(", Toast.LENGTH_SHORT).show();
                            }
                            loadTextBtnUsuarios(btnUsuarios, usuarioList.size());
                        }else{
                            Toast.makeText(view.getContext(), "Ya asistes a la pichanga", Toast.LENGTH_SHORT).show();
                        }
                        view.setVisibility(View.GONE);
                    }
                });

                btnRechazar = (ImageButton) v.findViewById(R.id.btnRechazar);
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Partido partido = partidoList.get(getAdapterPosition());
                        List<Usuario> usuarioList = partido.getUsuarioList();
                        Usuario deleted = null;
                        if(usuarioList.size() > 0){
                            for (Usuario u: usuarioList) {
                                String uNombre = u.getUsuario();
                                if (uNombre.equals(usuario)){
                                    deleted = u;
                                }
                            }
                            if (deleted != null){
                                usuarioList.remove(deleted);
                            }
                            view.setVisibility(View.GONE);
                            loadTextBtnUsuarios(btnUsuarios, usuarioList.size());
                            Toast.makeText(view.getContext(), "Dejas de asistir a la pichanga :(", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(view.getContext(), "Rechazado :(", Toast.LENGTH_SHORT).show();
                            view.setVisibility(View.GONE);
                        }
                    }
                });

                btnUsuarios = (Button) v.findViewById(R.id.btnUsuarios);
                btnUsuarios.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Partido partido = partidoList.get(getAdapterPosition());
                        List<Usuario> usuarioList = partido.getUsuarioList();
                        Toast.makeText(view.getContext(), Integer.toString(usuarioList.size()), Toast.LENGTH_SHORT).show();

                    }
                });
            }

        }

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_partido, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Partido partido = partidoList.get(position);
        holder.nombre.setText(partido.getNombrePartido());
        holder.fecha.setText(partido.getFechaCreacion());
        holder.hora.setText(partido.getHora());
        int cantidadUsuarios = partido.getUsuarioList().size();
        if(cantidadUsuarios == 14){
            holder.btnAceptar.setVisibility(View.GONE);
        }
        holder.btnUsuarios.setText(getTextBotonUsuarios(cantidadUsuarios));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), FichaPartidoActivity.class);
                i.putExtra("partido", partido);
                v.getContext().startActivity(i);
                Toast.makeText(v.getContext(), partido.getNombrePartido(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return partidoList.size();
    }

    private String getTextBotonUsuarios(int cantidad){
        if(cantidad < 14){
            return Integer.toString(cantidad) + "/14";
        }else{
            return "LLENO";
        }
    }

    public void loadTextBtnUsuarios(Button boton, int cantidad){
        if(cantidad < 14){
            boton.setText(Integer.toString(cantidad) + "/14");
        }else{
            boton.setText("LLENO");
        }
    }

}
