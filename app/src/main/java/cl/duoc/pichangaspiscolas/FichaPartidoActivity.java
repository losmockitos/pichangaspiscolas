package cl.duoc.pichangaspiscolas;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import cl.duoc.pichangaspiscolas.Clases.Partido;


public class FichaPartidoActivity extends AppCompatActivity {

    private EditText txtNombrePartido, txtFecha, txtHora;

    private Button btnAgregar, btnEliminar;

    Calendar fechaPartido, horaPartido;
    int dia, mes, anio, hora, minuto;

    String formatoHora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ficha_partido);

        txtNombrePartido = (EditText)findViewById(R.id.etNombreLugar);
        txtFecha = (EditText)findViewById(R.id.etFecha);
        txtHora = (EditText)findViewById(R.id.etHora);
        btnAgregar = (Button)findViewById(R.id.btnCrearMod);
        btnEliminar = (Button)findViewById(R.id.btnEliminar);

        fechaPartido = Calendar.getInstance();
        horaPartido = Calendar.getInstance();

        dia = fechaPartido.get(Calendar.DAY_OF_MONTH);
        mes = fechaPartido.get(Calendar.MONTH);
        anio = fechaPartido.get(Calendar.YEAR);
        hora = horaPartido.get(Calendar.HOUR_OF_DAY);
        minuto = horaPartido.get(Calendar.MINUTE);

        mes = mes+1;

        formatoHoraPartido(hora);

        txtFecha.setText(dia+"/"+mes+"/"+anio);
        txtHora.setText(hora + " : " + minuto + " " + formatoHora );

        txtFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(FichaPartidoActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        monthOfYear = monthOfYear+1;
                        txtFecha.setText(dayOfMonth+"/"+monthOfYear+"/"+year);

                    }
                }, anio, mes, dia);
                datePickerDialog.show();
            }
        });

        txtHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(FichaPartidoActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hora, int minuto) {
                        formatoHoraPartido(hora);
                        txtHora.setText(hora + " : " + minuto + " " + formatoHora);
                    }
                }, hora, minuto, true);
                timePickerDialog.show();
            }
        });

        btnAgregar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                agregarPartido();
                Intent i = new Intent(FichaPartidoActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        Intent i = getIntent();
        if (i != null){
            Partido partido = i.getParcelableExtra("partido");
            if (partido != null){
                loadPartido(partido);
            }
        }
    }

    private void formatoHoraPartido (int hora){

        if (hora == 0){
            hora += 12;
            formatoHora = "AM";
        }else if (hora == 12 ){
            formatoHora = "PM";
        }else if (hora > 12){
            hora -= 12;
            formatoHora = "PM";
        }else {
            formatoHora = "AM";
        }
    }

    private void loadPartido(Partido partido){
        txtNombrePartido.setText(partido.getNombrePartido());
        txtFecha.setText(partido.getFechaCreacion());
        txtHora.setText(partido.getHora());
    }

    private void agregarPartido() {

        boolean validaPartido = true;
        if(txtNombrePartido.getText().toString().length()<1){
            txtNombrePartido.setError("Ingrese Nombre");
            validaPartido = false;
        }

        if(txtFecha.getText().toString().length()<1){
            txtFecha.setError("Ingrese Fecha");
            validaPartido = false;
        }

        if(txtHora.getText().toString().length()<1){
            txtHora.setError("Ingrese Hora");
            validaPartido = false;
        }

        if(validaPartido){
            Partido p = new Partido();
            p.setNombrePartido(txtNombrePartido.getText().toString());
            p.setFechaCreacion(txtFecha.getText().toString());
            p.setHora(txtHora.getText().toString());

            if(BaseDeDatos.isAgregarPartido(p)){
                Toast.makeText(FichaPartidoActivity.this, "Partido " + p.getNombrePartido() + " creado correctamente", Toast.LENGTH_LONG).show();
                //limpiaCajasDeTexto();
            }else{
                Toast.makeText(FichaPartidoActivity.this, "El Partido ya existe", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
