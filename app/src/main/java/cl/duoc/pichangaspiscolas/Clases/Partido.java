package cl.duoc.pichangaspiscolas.Clases;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 11-11-17.
 */

public class Partido implements Parcelable{

    private String nombrePartido;
    private String fechaCreacion;
    private String hora;
    private List<Usuario> usuarioList = new ArrayList<>();

    public Partido() {
    }

    public Partido(String nombrePartido, String fechaCreacion, String hora) {
        this.nombrePartido = nombrePartido;
        this.fechaCreacion = fechaCreacion;
        this.hora = hora;
    }

    public String getNombrePartido() {
        return nombrePartido;
    }

    public void setNombrePartido(String nombrePartido) {
        this.nombrePartido = nombrePartido;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public boolean agregarUsuario(Usuario usuario){
        if(usuarioList.size() < 14){
            this.usuarioList.add(usuario);
            return true;
        }else{
            return false;
        }
    }

    public Partido(Parcel in){
        String[] data = new String[3];

        in.readStringArray(data);
        // the order needs to be the same as in writeToParcel() method
        this.nombrePartido = data[0];
        this.fechaCreacion = data[1];
        this.hora = data[2];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.nombrePartido,
                this.fechaCreacion,
                this.hora});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Partido createFromParcel(Parcel in) {
            return new Partido(in);
        }

        public Partido[] newArray(int size) {
            return new Partido[size];
        }
    };
}
